<!-- Sidebar -->
<div class="sidebar" id="sidebar">
    <div class="sidebar-inner slimscroll">
        <div id="sidebar-menu" class="sidebar-menu">
            <ul>
                <li>
                    <a href="{{route('adminDashboard')}}"><i class="la la-dashboard"></i> <span> Dashboard</span> </a>
                </li>
                <li>
                    <a href="{{route('category.index')}}"><i class="la la-list-alt"></i> <span> Category</span> </a>
                </li>

            </ul>
        </div>
    </div>
</div>
<!-- /Sidebar -->
