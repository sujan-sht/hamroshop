<?php

namespace Database\Seeders;
use App\Models\Admin;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Admin::insert([
            'name' => "Sujan Shrestha",
            'email' => "sujanstha016@gmail.com",
            'password' => bcrypt("password"),
            'address' => "Thimi",
            'image' => '',
            'phone' => "9810185213",
            'role_id' => 1,
            'status' => 1,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
    }
}
