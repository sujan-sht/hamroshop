@extends('admin.includes.admin_design')

@section('title') All Categories - {{ config('app.name', 'Laravel') }} @endsection

@section('content')
    <!-- Page Wrapper -->
    <div class="page-wrapper">
        <div class="content container-fluid">

            <!-- Page Header -->
            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <h3 class="page-title">All Categories</h3>
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{route('adminDashboard')}}">Dashboard</a></li>
                            <li class="breadcrumb-item active">All Categories</li>
                        </ul>
                    </div>
                    <div class="col-auto float-right ml-auto">
                        <a href="{{route('addCategory')}}" class="btn add-btn" ><i class="fa fa-plus"></i> Add Category</a>
                    </div>
                </div>
            </div>
            <!-- /Page Header -->

            <div class="row">
                <div class="col-sm-12">
                    <div class="card mb-0">
                        <div class="card-body">

                            <div class="table-responsive">
                                <table class="datatable table table-stripped mb-0">
                                    <thead>
                                    <tr>
                                        <th>S.N</th>
                                        <th>Category Image</th>
                                        <th>Category Name</th>
                                        <th>Category Code</th>
                                        <th>Main Category</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($categories as $category)
                                    <tr>
                                        <td>{{$loop -> index + 1}}</td>
                                        <td>
                                            @if(!empty($category->image))
                                                <img src="{{asset('public/uploads/category/'.$category->image)}}" width="50px">
                                            @else
                                                <img src="{{asset('public/uploads/default/no_image.png')}}" width="50px">

                                            @endif
                                        </td>
                                        <td>{{$category->category_name}}</td>
                                        <td>{{$category->category_id}}</td>
                                        <td>
                                            @if($category->parent_id==0)
                                                Main Category
                                            @else
                                                {{$category->subCategory->category_name}}
                                            @endif
                                        </td>
                                        <td>
                                            @if($category->status==1)
                                                <span class="badge bg-success">Active</span>
                                            @else
                                                <span class="badge bg-danger">In Active</span>
                                            @endif
                                        </td>
                                        <td>
                                            <button class="btn btn-info btn-sm"><i class="fa fa-pencil"></i></button>
                                            <button class="btn btn-success btn-sm"><i class="fa fa-eye"></i></button>
                                            <button class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>

                                        </td>
                                    </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <!-- /Page Wrapper -->


@endsection

@section('js')
    <!-- Datatable JS -->
    <script src="{{asset('public/adminpanel/assets/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('public/adminpanel/assets/js/dataTables.bootstrap4.min.js')}}"></script>
@endsection
