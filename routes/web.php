<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::prefix('/admin')->group(function(){
    //Admin Login
    Route::match(['get','post'],'/login','AdminLoginController@adminLogin')->name('adminLogin');

    Route::group(['middleware'=>['admin']],function(){
        //Admin dashboard
        Route::get('/dashboard','AdminLoginController@dashboard')->name('adminDashboard');
        //Admin profile
        Route::get('/profile','AdminProfileController@profile')->name('profile');
        //Admin Update
        Route::post('/profile/update/{id}','AdminProfileController@updateProfile')->name('updateProfile');
        //Change Password
        Route::get('profile/change_password','AdminProfileController@changePassword')->name('changePassword');
        //check current password
        Route::post('/profile/check_password','AdminProfileController@chkUserPassword')->name('chkUserPassword');
        //update admin password
        Route::post('/profile/update_password/{id}','AdminProfileController@updatePassword')->name('updatePassword');

        //category routes
        Route::get('/category','categoryController@category')->name('category.index');
        //add category
        Route::get('/category/add','categoryController@addCategory')->name('addCategory');
        //store category
        Route::post('/category/add','categoryController@storeCategory')->name('storeCategory');

    });

});

Route::get('/admin/forgot_password','AdminLoginController@forgotPassword')->name('forgotPassword');

Route::get('/admin/logout','AdminLoginController@adminLogout')->name('adminLogout');


