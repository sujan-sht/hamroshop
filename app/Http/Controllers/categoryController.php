<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

class categoryController extends Controller
{
    //category index
    public function category(){
        $categories = Category::latest()->get();
        return view('admin.category.index',compact('categories'));
    }

    //add category
    public function addCategory(){
        $categories = Category::where('parent_id',0)->orderBy('category_name','ASC')->get();
        return view('admin.category.add',compact('categories'));
    }

    //store Category
    public function storeCategory(Request $request){
        $data=$request->all();
        $validateData= $request->validate([
            'category_name' => 'required|max:255',
            'category_code' => 'required'
        ]);
        $category = new Category();
        $category->category_name = ucwords(strtolower($data['category_name']));
        $category->category_id = $data['category_code'];
        $category->slug = Str::slug('category_name');
        $category->parent_id = $data['parent_id'];
        if(empty($data['description'])){
            $category->description = "";
        } else {
            $category->description = $data['description'];
        }
        if(empty($data['status'])){
            $category->status = 0;
        } else {
            $category->status = 1;
        }
        $random = Str::random(10);
        if($request -> hasFile('image')){
            $image_tmp=$request->file('image');
            if($image_tmp->isValid()){
                $extension = $image_tmp->getClientOriginalExtension();
                $filename = $random .'.' . $extension;
                $image_path='public/uploads/category/' . $filename;
                Image::make($image_tmp)->save($image_path);
                $category->image = $filename;
            }
        }
        $category->save();
        Session::flash('success_message','Category Has Been Successfully added');
        return redirect()->back();

    }
}
